# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

git clone https://sklickovic@bitbucket.org/sklickovic/stroboskop.git

Naloga 6.2.3:
https://bitbucket.org/sklickovic/stroboskop/commits/5f0d65e39bf0d173c893d13ad5e4e1548f4ed504

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/sklickovic/stroboskop/commits/df9c639773beae4765a5b376ff5288797e6e6ea8

Naloga 6.3.2:
https://bitbucket.org/sklickovic/stroboskop/commits/eba154f004a23bb7f72db3c79ffa8ef89722412e

Naloga 6.3.3:
https://bitbucket.org/sklickovic/stroboskop/commits/5ab2e6ef97ac3b812f1b97cf906d16bbff468f03

Naloga 6.3.4:
https://bitbucket.org/sklickovic/stroboskop/commits/958c60126bb4dac48253199cf89208dbcfeb0352

Naloga 6.3.5:

git checkout master
git merge master izgled
git push -u origin master

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/sklickovic/stroboskop/commits/d0a9d89d602c59f26393d4fe1a9fa25c8f4a7e96

Naloga 6.4.2:
https://bitbucket.org/sklickovic/stroboskop/commits/8f32e8e788890d624798d9f9cf3c90de54a9fca7

Naloga 6.4.3:
https://bitbucket.org/sklickovic/stroboskop/commits/d9a437e6b17f33ed0e7add803cbe80fa3b1fcf70

Naloga 6.4.4:
https://bitbucket.org/sklickovic/stroboskop/commits/5b2f735ec1732a32b68493dac9d724e8e99d681d